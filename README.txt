Remove 3rd column from a csv file

cut -f1,2,4- -d, test.csv > test_less_lang.csv

Count the number of columns from a csv file

awk -F',' '{print NF;exit}' train.csv
